﻿using System;

// ReSharper disable InconsistentNaming
#pragma warning disable SA1611

namespace QuickSort
{
    public static class Sorter
    {
        public static void QuickSort(this int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0 || array.Length == 1)
            {
                return;
            }

            int startIndex = 0;
            int endIndex = array.Length - 1;
            int stackIndex = -1;
            int[] stack = new int[array.Length];

            stack[++stackIndex] = startIndex;
            stack[++stackIndex] = endIndex;

            while (stackIndex >= 0)
            {
                endIndex = stack[stackIndex--];
                startIndex = stack[stackIndex--];

                int p = Partition(ref array, startIndex, endIndex);

                if (p - 1 > startIndex)
                {
                    stack[++stackIndex] = startIndex;
                    stack[++stackIndex] = p - 1;
                }

                if (p + 1 < endIndex)
                {
                    stack[++stackIndex] = p + 1;
                    stack[++stackIndex] = endIndex;
                }
            }
        }

        private static int Partition(ref int[] data, int left, int right)
        {
            int temp;
            int x = data[right];
            int i = left - 1;

            for (int j = left; j <= right - 1; ++j)
            {
                if (data[j] <= x)
                {
                    i++;
                    temp = data[i];
                    data[i] = data[j];
                    data[j] = temp;
                }
            }

            temp = data[i + 1];
            data[i + 1] = data[right];
            data[right] = temp;

            return i + 1;
        }

        public static void RecursiveQuickSort(this int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            RecursiveQuickSort(array, 0, array.Length - 1);
        }

        public static void RecursiveQuickSort(int[] array, int leftIndex, int rightIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                return;
            }

            int i = leftIndex, j = rightIndex, pivot = array[i];
            while (i <= j)
            {
                while (array[i] < pivot)
                {
                    i++;
                }

                while (array[j] > pivot)
                {
                    j--;
                }

                if (i <= j)
                {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    i++;
                    j--;
                }
            }

            if (leftIndex < j)
            {
                RecursiveQuickSort(array, leftIndex, j);
            }

            if (i < rightIndex)
            {
                RecursiveQuickSort(array, i, rightIndex);
            }
        }
    }
}
